# What's New

## v3.0 - 2020 Jul 19
- Separate versions for armv7 and armv8
- Support run-time permissions
- Updated set of emojis
- Option to set permanent emoji key
- Option to turn on number row
- Several bugfixes
- New dictionaries for Kashmiri, Marathi, Maithili, Odia, Sanskrit, Santali, Assamese

## v2.1 - 2018 Jul 22
- Support for Santali Ol Chiki
- Add missing characters in Bengali layouts
- Make Devanagari Danda easily accessible in Hindi and Punjabi layouts
- Add missing characters in Assamese layouts
- Add ḥarakāt in Arabic keyboard
- Add nukta in all Odiya layouts
- Add ZWNJ to all Tamil and Malayalam layouts
