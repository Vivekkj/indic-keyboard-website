module.exports = {
  title: 'Indic Keyboard',
  dest: 'public',
  themeConfig: {
    logo: '/logo.png',
    repo: 'https://gitlab.com/indicproject/indic-keyboard',
    repoLabel: 'Source Code',
    docsRepo: 'https://gitlab.com/indicproject/indic-keyboard-website',
    editLinks: true,
    editLinkText: 'Help us improve this page!',
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Download', link: '/#download' },
      { text: 'FAQ', link: '/faq' },
      { text: 'What\'s New', link: '/whats-new' },
      { text: 'Contact Us', link: '/#contact-us' },
    ]
  }
}
